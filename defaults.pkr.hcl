variable "tf_version" {
  type=string
  default="0.13.4"
}


locals {
  tf_download_url = "https://releases.hashicorp.com/terraform/${var.tf_version}/terraform_${var.tf_version}_linux_amd64.zip"
  tf_checksums_src = "terraform_${var.tf_version}_SHA256SUMS"
}
