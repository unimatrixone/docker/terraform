# Terraform Docker Image

This Packer template builds Docker images on various Linux distributions
with Terraform installed.


# Prerequisites

- Docker
- Packer v1.6.1 or higher (build only).
- Push access to the configured Docker repository (build only).


# Using

- On the command-line: `docker run -it registry.gitlab.com/unimatrixone/docker/terraform:latest`
- In a GitLab pipeline:

  ```
  stages:
  - setup
  - provision
  - teardown

  Setup
    # Be aware that that the terraform.tfstate file may contain plain secrets,
    # so either configure a backend or do not use secrets inside the plan.
    stage: setup
    artifacts:
      untracked: true
    script:
    - terraform init

  Apply:
    stage: provision
    image: registry.gitlab.com/unimatrixone/docker/terraform:0.13.0-alpine3.12
    artifacts:
      untracked: true
    script:
    - terraform apply -auto-approve

  Destroy:
    stage: teardown
    image: registry.gitlab.com/unimatrixone/docker/terraform:0.13.0-alpine3.12
    script:
    - terraform destroy -auto-approve
  ```


# Building

Create the file `local.pkr.hcl` with the appropriate values:

```
locals {
  docker_repository = "<replace with your Docker repository>"
}
```

Alternatively, the Docker repository may be provided through the command
line with the `-var 'docker_repository='"<your Docker repository>"'` parameter
(note the quoting).

Run the following command to build

```
packer build -var tf_version=<replace with Terraform version> .
```


# License

MIT/BSD


# Author information

This template was created by **Cochise Ruhulessin** for the
[Unimatrix One](https://cloud.unimatrixone.io) platform.

- [Send me an email](mailto:cochise.ruhulessin@unimatrixone.io)
- [GitHub](https://github.com/cochiseruhulessin)
- [LinkedIn](https://www.linkedin.com/in/cochise-ruhulessin-0b48358a/)
