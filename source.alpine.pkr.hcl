source "docker" "alpine" {
  image   = "alpine:3.12"
  commit  = true
  changes = [
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}",
    "ENTRYPOINT [\"terraform\"]"
  ]
}


source "docker" "gitlab" {
  image   = "alpine:3.12"
  commit  = true
  changes = [
    "ENTRYPOINT [\"\"]",
    "CMD []"
  ]
}
