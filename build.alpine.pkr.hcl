variable "docker_repository" { default = null }


build {
  sources = [
    "source.docker.alpine",
    "source.docker.gitlab"
  ]

  provisioner "file" {
    source = "files/${local.tf_checksums_src}"
    destination = "/tmp/${local.tf_checksums_src}"
  }

  provisioner "shell" {
    inline = [
      "addgroup -g ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} -u ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/ash ${local.runtime_usr} -D",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "apk add --no-cache curl git make"
    ]
    only = ["docker.alpine", "docker.gitlab"]
  }

  provisioner "shell" {
    inline = [
        "cd /tmp",
        "wget ${local.tf_download_url}",
        "sed -i '/.*linux_amd64.zip$/!d' ${local.tf_checksums_src}",
        "sha256sum -c ${local.tf_checksums_src}",
        "unzip terraform_${var.tf_version}*.zip",
        "chmod +x terraform",
        "mv terraform /usr/local/bin/terraform",
        "rm -rf terraform*"
      ]
  }

  provisioner "shell" {
    inline = [
      "rm -rf /var/lib/apk/* /var/cache/apk/* /etc/apk/cache/*",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
    ]
    only = ["docker.alpine", "docker.gitlab"]
  }

  provisioner "shell" {
    inline = [
      "rm -rf /tmp/*",
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        join("-", [var.tf_version, "alpine3.12"]),
        join("-", [regex_replace(var.tf_version, "\\.[\\d]+$", ""), "alpine3.12"]),
      ]
      only        = ["docker.alpine"]
    }

    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        join("-", [var.tf_version, "gitlab"]),
        join("-", [regex_replace(var.tf_version, "\\.[\\d]+$", ""), "gitlab"]),
      ]
      only        = ["docker.gitlab"]
    }

    post-processor "docker-push" {}
  }
}
