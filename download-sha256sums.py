#!/usr/bin/env python3
import os

import requests


BASE_URL = "https://releases.hashicorp.com/terraform"
VERSIONS = [
    "1.0.9",
    "1.0.8",
    "1.0.7",
    "1.0.6",
    "1.0.5",
    "1.0.4",
    "1.0.3",
    "1.0.2",
    "1.0.1",
    "1.0.0",
    "0.15.5",
    "0.15.4",
    "0.15.3",
    "0.15.2",
    "0.15.1",
    "0.15.0",
    "0.14.11",
    "0.14.10",
    "0.14.9",
    "0.14.8",
    "0.14.7",
    "0.14.6",
    "0.14.5",
    "0.14.4",
    "0.14.3",
    "0.14.2",
    "0.14.1",
    "0.14.0",
    "0.13.7",
    "0.13.6",
    "0.13.5",
    "0.13.4",
    "0.13.3",
    "0.13.2",
    "0.13.1",
    "0.12.31",
]


def main():
    for version in VERSIONS:
        fn = f'terraform_{version}_SHA256SUMS'
        sums_url = f'{BASE_URL}/{version}/{fn}'
        if os.path.exists(os.path.join('files', fn)):
            continue
        print(sums_url)
        response = requests.get(sums_url)
        with open(os.path.join('files', fn), 'wb') as f:
            f.write(response.content)


if __name__ == '__main__':
    main()
